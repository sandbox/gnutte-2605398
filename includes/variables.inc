<?php
/**
 * @file
 * Variable type.
 */

/**
 * List variable's actions.
 */
function drupal_env_get_variables_actions() {
  return array(
    'set' => t('Set'),
    'delete' => t('Del'),
  );
}

/**
 * Get action form.
 */
function drupal_env_get_variables_configuration_action_form($action) {
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Variable name'),
    '#default_value' => $action['config']['name'],
  );

  switch ($action['action']) {
    case 'set':
      $form['value'] = array(
        '#type' => 'textfield',
        '#title' => t('Variable value'),
        '#default_value' => $action['config']['value'],
      );
      break;
  }

  return $form;
}

/**
 * Execute the action set.
 */
function drupal_env_variables_configuration_action_set(&$configuration, &$errors) {
  if (!isset($configuration['name']) || empty($configuration['name'])) {
    $errors[] = t('There is no variable name defined');
    return FALSE;
  }

  if (!isset($configuration['value']) || empty($configuration['value'])) {
    $errors[] = t('There is no value to be set');
    return FALSE;
  }

  $configuration['message'] = $configuration['name'];
  variable_set($configuration['name'], $configuration['value']);

  return TRUE;
}

/**
 * Execute the action delete.
 */
function drupal_env_variables_configuration_action_delete(&$configuration, &$errors) {
  if (!isset($configuration['name']) || empty($configuration['name'])) {
    $errors[] = t('There is no variable name defined');
    return FALSE;
  }

  $configuration['message'] = $configuration['name'];
  variable_del($configuration['name']);

  return TRUE;
}
