<?php
/**
 * @file
 * Module type.
 */

/**
 * List variable's actions.
 */
function drupal_env_get_modules_actions() {
  return array(
    'enable' => t('Enable'),
    'disable' => t('Disable'),
  );
}

/**
 * Get action form.
 */
function drupal_env_get_modules_configuration_action_form($action) {
  $form = $modules = array();
  $available = system_rebuild_module_data();

  switch ($action["action"]) {
    case 'enable':
      foreach ($available as $name => $module) {
        if (!$module->status) {
          $modules[$name] = isset($module->info['name']) ? $module->info['name'] : $name;
        }
      }
      break;
    case 'disable':
      foreach ($available as $name => $module) {
        if ($module->status) {
          $modules[$name] = isset($module->info['name']) ? $module->info['name'] : $name;
        }
      }
      break;
  }

  if (!empty($modules)) {
    $form['modules'] = array(
      '#type' => 'select',
      '#title' => t('Modules'),
      '#options' => array('none' => t('Select')) + $modules,
      '#multiple' => TRUE,
      '#default_value' => $action['config']['modules'],
    );
  }
  return $form;
}

/**
 * Execute the action enable.
 */
function drupal_env_modules_configuration_action_enable(&$configuration, &$errors) {
  if (!isset($configuration['modules']) || !is_array($configuration['modules'])) {
    $errors[] = t('There is no module to enable');
    return FALSE;
  }

  $modules = array_keys($configuration['modules']);

  $configuration['message'] = implode(' | ', $modules);
  module_enable($modules);

  return true;
}

/**
 * Execute the action disable.
 */
function drupal_env_modules_configuration_action_disable(&$configuration, &$errors) {
  if (!isset($configuration['modules']) || !is_array($configuration['modules'])) {
    $errors[] = t('There is no module to disable');
    return FALSE;
  }

  $modules = array_keys($configuration['modules']);

  $configuration['message'] = implode(' | ', $modules);
  module_disable($modules);

  return TRUE;
}
