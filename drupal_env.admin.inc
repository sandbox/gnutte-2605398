<?php
/**
 * @file
 * Admin configuration file.
 */

/**
 * Admin environment list form.
 */
function drupal_env_admin_form($form, &$form_state) {
  $form['env'] = array('#tree' => TRUE);

  $environments = db_select('drupal_environments', 'e')->fields('e')->orderBy('weight')->execute()->fetchAll();
  foreach ($environments as $environment) {
    $operations = array(
      l(t('Edit'), 'admin/config/system/env/' . $environment->id . '/edit', array('query' => array('destination' => current_path()))),
      l(t('Delete'), 'admin/config/system/env/' . $environment->id . '/delete', array('query' => array('destination' => current_path())))
    );

    $form['env'][$environment->id] = array();
    $form['env'][$environment->id]['label'] = array('#markup' => $environment->label);
    $form['env'][$environment->id]['machine_name'] = array('#markup' => $environment->machine_name);
    $form['env'][$environment->id]['weight'] = array('#type' => 'weight', '#title' => t('Weight'), '#title_display' => 'invisible', '#default_value' => $environment->weight);
    $form['env'][$environment->id]['op'] = array('#markup' => implode(" | ", $operations));
  }

  $form['add'] = array('#markup' => l(t('Add'), 'admin/config/system/env/add', array('query' => array('destination' => current_path()))));

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  return $form;
}

/**
 * Admin environment list form submit.
 */
function drupal_env_admin_form_submit(&$form, &$form_state) {
  foreach ($form_state['values']['env'] as $id => $environment) {
    db_update('drupal_environments')
        ->fields(array('weight' => $environment['weight']))
        ->condition('id', $id)
        ->execute();
  }

  drupal_set_message(t("The configuration has been saved."));
}

/**
 * Admin environment edit form.
 */
function drupal_env_edit_form($form, &$form_state, $environment = null) {
  if (!isset($form_state['values'])) {
    $form_state['values'] = array();
    $form_state['values']['env'] = !is_null($environment) ? $environment : array(
      'id' => null,
      'label' => '',
      'machine_name' => '',
      'weight' => 0,
    );

    if (!isset($form_state['values']['env']['config'])) {
      $form_state['values']['env']['config'] = array(
        array(
          'type' => 'none',
          'action' => 'none',
          'config' => '',
        ),
      );
    }
  }

  $form['env'] = array('#tree' => TRUE);
  $form['env']['id'] = array(
    '#type' => 'hidden',
    '#value' => $form_state['values']['env']['id'],
  );
  $form['env']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $form_state['values']['env']['label'],
    '#required' => TRUE,
  );
  $form['env']['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#default_value' => $form_state['values']['env']['machine_name'],
    '#required' => TRUE,
  );

  $ajax_callback = array(
    'callback' => 'drupal_env_edit_form_ajax_callback',
    'wrapper' => 'drupal-env-config-list',
  );

  $config = $form_state['values']['env']['config'];

  $form['env']['config'] = array(
    '#prefix' => "<div id='drupal-env-config-list'>",
    '#suffix' => '</div>',
  );

  foreach ($config as $key => $item) {
    $form['env']['config'][$key]['type'] = array(
      '#type' => 'select',
      '#title' => t('Item type'),
      '#options' => array("none" => t('Select')) + drupal_env_get_supported_types(),
      '#default_value' => $item['type'],
      '#ajax' => $ajax_callback,
    );
    $form['env']['config'][$key]['action'] = array(
      '#type' => 'select',
      '#title' => t('Action'),
      '#options' => array("none" => t('Select')) + drupal_env_get_type_actions($item['type']),
      '#default_value' => $item['action'],
      '#ajax' => $ajax_callback,
    );
    $form['env']['config'][$key]['config'] = drupal_env_get_type_configuration_form($item);
    $form['env']['config'][$key]['remove'] = array(
      '#type' => 'submit',
      '#value' => t('Remove item'),
      '#ajax' => $ajax_callback,
      '#submit' => array('drupal_env_edit_form_remove_submit'),
    );
  }

  $form['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add item'),
    '#ajax' => $ajax_callback,
    '#submit' => array('drupal_env_edit_form_add_submit'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Admin environment edit form add item submit.
 */
function drupal_env_edit_form_add_submit($form, &$form_state) {
  $form_state['values']['env']['config'][] = array(
    'type' => 'none',
    'action' => 'none',
    'config' => '',
  );
  $form_state['rebuild'] = TRUE;
}

/**
 * Admin environment edit form remove item submit.
 */
function drupal_env_edit_form_remove_submit($form, &$form_state) {
  $id = $form_state['clicked_button']['#parents'][2];

  unset($form_state['values']['env']['config'][$id]);
  $form_state['rebuild'] = TRUE;
}

/**
 * Admin environment edit form ajax callback.
 */
function drupal_env_edit_form_ajax_callback($form, $form_state) {
  return $form['env']['config'];
}

/**
 * Admin environment edit form submit.
 */
function drupal_env_edit_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  $primary = array();
  $env = new stdClass();

  if (!is_null($values['env']['id'])) {
    $primary[] = 'id';
    $env->id = $values['env']['id'];
  }

  $env->label = $values['env']['label'];
  $env->machine_name = $values['env']['machine_name'];
  $env->config = isset($values['env']['config']) ? $values['env']['config'] : array();

  drupal_write_record('drupal_environments', $env, $primary);

  drupal_set_message(t("The environment has been saved."));
}

/**
 * Admin environment delete form.
 */
function drupal_env_delete_form($form, &$form_state, $environment) {
  $form['env'] = array('#type' => 'hidden', '#value' => $environment['id']);
  return confirm_form($form, t("Do you want to delete this environment ?"), "admin/configuration/system/env");
}

/**
 * Admin environment delete form submit.
 */
function drupal_env_delete_form_submit(&$form, &$form_state) {
  db_delete('drupal_environments')->condition('id', $form_state['values']['env'])->execute();

  drupal_set_message(t("The environment has been deleted."));
}
