<?php
/**
 * @file
 * Module file.
 */

/**
 * Implements hook_drush_command().
 */
function drupal_env_drush_command() {
  $items = array();

  $items['env-set'] = array(
    'callback' => 'drupal_env_drush_set',
    'description' => dt('Set a preconfigured configuration environment'),
    'aliases' => array('es'),
    'examples' => array(
      'drush env-set prod' => 'Set a production environment',
      'drush env-set dev' => 'Set a development environment',
    ),
    'arguments' => array(
      'environment' => "Required. The id of the environment."
    )
  );

  $items['env-list'] = array(
    'callback' => 'drupal_env_drush_list',
    'description' => dt('List the preconfigured configuration environment'),
    'aliases' => array('el'),
    'examples' => array(
      'drush env-reset' => 'List the environment'
    ),
    'arguments' => array()
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function drupal_env_drush_help($section) {
  switch ($section) {
    case 'drush:env-set':
      return dt("Set a development environment");
    case 'drush:env-list':
      return dt("List the development environment");
  }
}

/**
 * Set an environment by drush.
 */
function drupal_env_drush_set($env = '') {
  $environments = db_select('drupal_environments', 'e')->fields('e')->condition('machine_name', $env)->execute()->fetchAll();
  if (empty($environments)) {
    return drush_set_error("error", t("The environment does not exist."));
  }

  $environment = array_shift($environments);

  $configuration = unserialize($environment->config);
  foreach ($configuration as $item) {
    module_load_include('inc', 'drupal_env', 'includes/' . $item['type']);

    $function_name = "drupal_env_" . $item['type'] . "_configuration_action_" . $item['action'];
    if (!function_exists($function_name)) {
      drush_log(t('!type - !action !module : !error', array('!type' => ucfirst($item['type']), '!action' => $item['action'], '!module' => $item['config']['modules'], '!error' => "Not implemented")), 'error');
      continue;
    }
    
    $errors = array();
    
    if ($function_name($item['config'], $errors)) {
      drush_log(t('!type - !action !module', array('!type' => ucfirst($item['type']), '!action' => $item['action'], '!module' => $item['config']['message'])), 'success');
    }
    else {
      drush_log(t('!type - !action !module : !error', array('!type' => ucfirst($item['type']), '!action' => $item['action'], '!module' => $item['config']['modules'], '!error' => implode(" | ", $errors))), 'error');
    }
  }
}

/**
 * List available environments by drush.
 */
function drupal_env_drush_list() {
  
}
